//package com.ghtk.handlingexceptions.exceptions.handling;
//
//import com.ghtk.handlingexceptions.controllers.responses.DefaultResponse;
//import com.ghtk.handlingexceptions.exceptions.ProjectException;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseStatus;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//
//@Slf4j
//@RestControllerAdvice
//public class ApiExceptionHandler {
//
//  @ExceptionHandler(ProjectException.class)
//  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
//  public DefaultResponse<Object> handleProjectException(ProjectException e) {
//    return DefaultResponse.error(e);
//  }
//}
