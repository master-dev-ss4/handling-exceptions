package com.ghtk.handlingexceptions.exceptions;

import com.ghtk.handlingexceptions.constants.ResponseStatusConstant;

public class ObjectNotFoundException extends ProjectException {

  public ObjectNotFoundException(ResponseStatusConstant e) {
    super(e.getCode(), e.getMessage());
  }
}
