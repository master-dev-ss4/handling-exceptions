package com.ghtk.handlingexceptions.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectException extends RuntimeException {

  private Integer code;
  private String message;
}
