package com.ghtk.handlingexceptions.constants;

import lombok.Getter;

@Getter
public enum ResponseStatusConstant {
  COMMON_FAIL(0, "Hệ thống đang gặp sự cố vui lòng thử lại sau."),
  SUCCESS(1, "Thành công."),

  NOT_FOUND_PROFILE(100, "Không tìm thấy profile.");

  private final int code;
  private final String message;

  ResponseStatusConstant(int code, String message) {
    this.code = code;
    this.message = message;
  }
}
