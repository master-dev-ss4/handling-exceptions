package com.ghtk.handlingexceptions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HandlingExceptionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(HandlingExceptionsApplication.class, args);
	}

}
