package com.ghtk.handlingexceptions.controllers;

import com.ghtk.handlingexceptions.controllers.responses.profile.ProfileResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/** API trả ra với đúng dữ liệu */
@RestController
@RequestMapping("/v1.0/profile")
@Slf4j
public class ProfileControllerV1 {

  @GetMapping("/{profileId}")
  public ResponseEntity<ProfileResponse> getProfileById(
      @PathVariable(name = "profileId") Long profileId) {
    return ResponseEntity.ok(new ProfileResponse(profileId));
  }
}
