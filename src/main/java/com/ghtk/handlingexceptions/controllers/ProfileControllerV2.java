package com.ghtk.handlingexceptions.controllers;

import com.ghtk.handlingexceptions.controllers.responses.profile.ProfileResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Api bắn ra exception do không tìm thấy profile với id gửi lên. Không có phần try catch xử lý
 * exception.
 */
@RestController
@RequestMapping("/v2.0/profile")
@Slf4j
public class ProfileControllerV2 {

  private ProfileResponse findProfileById(Long profileId) {
    throw new RuntimeException("Không tìm thấy profile có id " + profileId);
  }

  @GetMapping("/{profileId}")
  public ResponseEntity<ProfileResponse> getProfileById(
      @PathVariable(name = "profileId") Long profileId) {
    return ResponseEntity.ok(findProfileById(profileId));
  }
}
