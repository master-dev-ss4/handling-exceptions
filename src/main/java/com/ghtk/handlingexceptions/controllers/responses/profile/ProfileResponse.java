package com.ghtk.handlingexceptions.controllers.responses.profile;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ProfileResponse {

  private Long id;
  private String code;

  public ProfileResponse(Long id) {
    this.id = id;
    this.code = "PROFILE_00001";
  }
}
