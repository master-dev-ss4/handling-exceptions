package com.ghtk.handlingexceptions.controllers;

import com.ghtk.handlingexceptions.constants.ResponseStatusConstant;
import com.ghtk.handlingexceptions.controllers.responses.DefaultResponse;
import com.ghtk.handlingexceptions.controllers.responses.profile.ProfileResponse;
import com.ghtk.handlingexceptions.exceptions.ObjectNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v5.0/profile")
@Slf4j
public class ProfileControllerV5 {

  private ProfileResponse findProfileById(Long profileId) {
    throw new ObjectNotFoundException(ResponseStatusConstant.NOT_FOUND_PROFILE);
  }

  @GetMapping("/{profileId}")
  public ResponseEntity<DefaultResponse<ProfileResponse>> getProfileById(
      @PathVariable(name = "profileId") Long profileId) {
    return ResponseEntity.ok(DefaultResponse.success(findProfileById(profileId)));
  }
}
